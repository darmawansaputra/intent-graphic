package com.darmawan.wpg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Register extends AppCompatActivity implements View.OnClickListener {
    TextView txtName, txtEmail, txtPhone, txtPassword;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setTitle("Register");

        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        txtPhone = findViewById(R.id.txtPhone);
        txtPassword = findViewById(R.id.txtPassword);

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnRegister:
                if(validate()) {
                    Intent intent = new Intent(Register.this, Login.class);
                    intent.putExtra("email", txtEmail.getText().toString());
                    startActivity(intent);
                }
                break;
        }
    }

    private boolean validate() {
        String err = "";
        if(txtName.getText().toString().equals(""))
            err += "Name cannot null";
        else if(txtEmail.getText().toString().equals(""))
            err += "Email cannot null";
        else if (!Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches())
            err += "Email is invalid";
        else if(txtPassword.getText().toString().equals(""))
            err += "Password cannot null";
        else if(txtPhone.getText().toString().equals(""))
            err += "Phone cannot null";

        if(!err.equals("")) {
            Toast.makeText(this, err, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
