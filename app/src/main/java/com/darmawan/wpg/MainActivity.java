package com.darmawan.wpg;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new MyView(this));
    }

    public class MyView extends View {
        public MyView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            Paint paint = new Paint();
            Path path = new Path();

            //circle
            int x = getWidth();
            int y = getHeight();
            int radius = 100;

            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.WHITE);

            canvas.drawPaint(paint);

            paint.setColor(Color.parseColor("#FF0000"));

            canvas.drawCircle(x/2, y/2, radius, paint);

            //Rectangle
            paint.setColor(Color.parseColor("#FF1000"));

            canvas.drawRect(100, 650, 450, 770, paint);

            //Triangle
            Point a = new Point(275, 80);
            Point b = new Point(100, 280);
            Point c = new Point(450, 280);

            path.setFillType(Path.FillType.EVEN_ODD);

            path.lineTo(a.x, a.y);
            path.lineTo(b.x, b.y);
            path.lineTo(c.x, c.y);
            path.lineTo(a.x, a.y);
            path.close();

            canvas.drawPath(path, paint);

            Drawable d = getResources().getDrawable(R.drawable.ic_lol, null);
            d.setBounds(150, 330, 400, 570);
            d.draw(canvas);
        }
    }
}
