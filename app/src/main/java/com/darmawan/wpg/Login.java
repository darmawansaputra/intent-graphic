package com.darmawan.wpg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends AppCompatActivity implements View.OnClickListener {
    TextView txtRegister, txtEmail, txtPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);

        txtRegister = findViewById(R.id.txtRegister);
        txtRegister.setOnClickListener(this);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        Intent in= getIntent();
        Bundle b = in.getExtras();

        if(b != null) {
            String j =(String) b.get("email");
            txtEmail.setText(j);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnLogin:
                if(validate()) {
                    Toast.makeText(this, "Successfully login", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Login.this, MainActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.txtRegister:
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
                break;
        }
    }

    private boolean validate() {
        String err = "";
        if(txtEmail.getText().toString().equals(""))
            err += "Email cannot null";
        else if (!Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches())
            err += "Email is invalid";
        else if(txtPassword.getText().toString().equals(""))
            err += "Password cannot null";

        if(!err.equals("")) {
            Toast.makeText(this, err, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
